package database;

import database.generated.GeneratedMydbApplication;

/**
 * An {@link com.speedment.runtime.core.ApplicationBuilder} interface for the
 * {@link com.speedment.runtime.config.Project} named mydb.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public interface MydbApplication extends GeneratedMydbApplication {}