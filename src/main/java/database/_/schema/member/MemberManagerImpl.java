package database._.schema.member;

import database._.schema.member.generated.GeneratedMemberManagerImpl;

/**
 * The default implementation for the manager of every {@link
 * database._.schema.member.Member} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class MemberManagerImpl 
extends GeneratedMemberManagerImpl 
implements MemberManager {}