package database._.schema.city;

import database._.schema.city.generated.GeneratedCityManagerImpl;

/**
 * The default implementation for the manager of every {@link
 * database._.schema.city.City} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class CityManagerImpl 
extends GeneratedCityManagerImpl 
implements CityManager {}