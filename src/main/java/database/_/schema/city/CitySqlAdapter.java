package database._.schema.city;

import database._.schema.city.generated.GeneratedCitySqlAdapter;

/**
 * The SqlAdapter for every {@link database._.schema.city.City} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public class CitySqlAdapter extends GeneratedCitySqlAdapter {}